jQuery(function() {
    ////////////////////////////////  sub menu sliding down and up ////////////////////////////
    jQuery("nav").find("li:first").click(function() {
        if (jQuery("#news-sub-menu").css("display") === "none") {
            jQuery(".sub-menu").hide();
            jQuery("#news-sub-menu").slideDown(500).css("display", "flex"); 
        } else {
            jQuery("#news-sub-menu").slideUp();
        }
    });
    jQuery("nav").find("li:nth-child(2)").click(function() {
        if (jQuery("#team-sub-menu").css("display") === "none") {
            jQuery(".sub-menu").hide();
            jQuery("#team-sub-menu").slideDown(500).css("display", "flex"); 
        } else {
            jQuery("#team-sub-menu").slideUp();
        }
    });
    jQuery("nav").find("li:nth-child(3)").click(function() {
        if (jQuery("#season-sub-menu").css("display") === "none") {
            jQuery(".sub-menu").hide();
            jQuery("#season-sub-menu").slideDown(500).css("display", "flex"); 
        } else {
            jQuery("#season-sub-menu").slideUp();
        }
    });
    jQuery("nav").find("li:nth-child(4)").click(function() {
        if (jQuery("#club-sub-menu").css("display") === "none") {
            jQuery(".sub-menu").hide();
            jQuery("#club-sub-menu").slideDown(500).css("display", "flex"); 
        } else {
            jQuery("#club-sub-menu").slideUp();
        }
    });
    jQuery("nav").find("li:nth-child(5)").click(function() {
        if (jQuery("#gal-sub-menu").css("display") === "none") {
            jQuery(".sub-menu").hide();
            jQuery("#gal-sub-menu").slideDown(500).css("display", "flex"); 
        } else {
            jQuery("#gal-sub-menu").slideUp();
        }
    });
    /////////////////////////////////////  scroll up the header  /////////////////////////////////
    jQuery(window).scroll(function() {
    //      // let clientHeight = jQuery(this).prop("clientHeight");
    //     // let scrollHeight = jQuery(this).prop("scrollHeight");

        let scrollTop = jQuery("html").prop("scrollTop");
        
        if (scrollTop > 0) {
            jQuery("header").stop().animate({"height": "3.6rem"}, 500);
            jQuery("#logo img").stop().animate({"height": "3.2rem", "width": "2.1rem"}, 500);
            jQuery("nav").stop().animate({"top": "3.6rem"}, 500);
            jQuery(".sub-menu").stop().animate({"top": "7.3rem"}, 500);
            jQuery("#news-slide-div").stop().animate({"top": "7.3rem"}, 500);
            jQuery("#other-content").stop().animate({"margin-top": "7.3rem"}, 500);
            jQuery("header").find("h2").stop().slideUp(500);
            jQuery(".com").stop().slideUp(500);
        } else {
            jQuery("header").stop().animate({"height": "10.5rem"}, 500);
            jQuery("#logo img").stop().animate({"height": "7.5rem", "width": "5rem"}, 500);
            jQuery("nav").stop().animate({"top": "10.5rem"}, 500);
            jQuery(".sub-menu").stop().animate({"top": "14.2rem"}, 500);
            jQuery("#news-slide-div").stop().animate({"top": "14.2rem"}, 500);
            jQuery("#other-content").stop().animate({"margin-top": "14-2rem"}, 500);
            jQuery("header").find("h2").stop().slideDown(500);
            jQuery(".com").stop().slideDown(500).css("display", "flex");
        }
    });
    ////////////////////////////////// sliding the image slide /////////////////////////////////////
    var firstImg = jQuery("#first-img");
    var secondImg = jQuery("#second-img");
    var thirdImg = jQuery("#third-img");
    
    jQuery("#right-arrow").click(function() {
        jQuery("#news-slide-div").css("overflow-x", "hidden");
        if (firstImg.css("left") === "0px") {
            firstImg.stop().animate({"left": "-100%"}, 500);
            secondImg.stop().animate({"left": "0"}, 500);
        } else if (secondImg.css("left") === "0px") {
            secondImg.stop().animate({"left": "-100%"}, 500);
            thirdImg.stop().animate({"left": "0"}, 500);
        }
    });

    jQuery("#left-arrow").click(function() { 
        jQuery("#news-slide-div").css("overflow-x", "hidden");
        if (secondImg.css("left") === "0px") {
            firstImg.stop().animate({"left": "0"}, 500);
            secondImg.stop().animate({"left": "100%"}, 500);
        } else if (thirdImg.css("left") === "0px") {
            secondImg.stop().animate({"left": "0"}, 500);
            thirdImg.stop().animate({"left": "100%"}, 500);
        } 
    }); 
});